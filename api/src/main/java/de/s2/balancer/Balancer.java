package de.s2.balancer;

import java.rmi.Remote;

public interface Balancer extends Remote {
	ClientConnection getNodeForService(Class<?> service) throws java.rmi.RemoteException;
}