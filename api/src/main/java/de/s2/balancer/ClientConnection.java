package de.s2.balancer;

import java.io.Serializable;
import java.rmi.Remote;

public interface ClientConnection extends Serializable {
	<T extends Remote> T getServiceInterface();
}
