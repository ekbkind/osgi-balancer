package de.s2.balancer;

import java.rmi.Remote;

public interface EchoService extends Remote {
	String echo(String text) throws java.rmi.RemoteException;
}
