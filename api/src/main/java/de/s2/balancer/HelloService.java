package de.s2.balancer;

import java.rmi.Remote;

public interface HelloService extends Remote {
	String hello(String msg) throws java.rmi.RemoteException;
}
