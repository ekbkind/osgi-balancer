package de.s2.balancer;

import java.rmi.Remote;

public interface Node extends Remote {
	void activateBundle(String name) throws java.rmi.RemoteException;
}
