package de.s2.balancer;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RMIClientConnector implements ClientConnection {

	private static final long serialVersionUID = 1L;

	public RMIClientConnector(String host, int port, String serviceName) {
		this.host = host;
		this.port = port;
		this.serviceName = serviceName;
	}

	private static final Logger logger = LoggerFactory
			.getLogger(RMIClientConnector.class);

	private String host;
	private int port;
	private String serviceName;

	public <T extends Remote> T getServiceInterface() {
		try {
			logger.info("creating service with name [{}]", serviceName);
			T result = createResultObject();
			return result;
		} catch (Exception e) {
			logger.error(
					"Error creating RMI Service stub for {}:{} with service {}",
					new Object[] { host, port, serviceName }, e);

		}
		return null;
	}

	private <T extends Remote> T createResultObject() throws RemoteException,
			NotBoundException, AccessException {
		Registry registry = LocateRegistry.getRegistry(host, port);
		@SuppressWarnings("unchecked")
		T result = (T) registry.lookup(serviceName);
		return result;
	}

}
