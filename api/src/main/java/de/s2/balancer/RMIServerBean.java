package de.s2.balancer;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * LocateRegistry has no destructor but needs one, otherwise tests and
 * features:install features:uninstall cycles fail upon second
 * {@link LocateRegistry#createRegistry}
 */
public class RMIServerBean {
	private java.rmi.registry.Registry registry;

	public RMIServerBean(int port) throws Exception {
		this.registry = java.rmi.registry.LocateRegistry.createRegistry(port);
	}

	public void preDestroy() throws Exception {
		UnicastRemoteObject.unexportObject(registry, true);
	}

	public Registry getRegistry() {
		return registry;
	}

	public <T extends Remote> void register(String service, T instance)
			throws Exception {
		registry.bind(service, UnicastRemoteObject.exportObject(instance, 0));
	}

	public void deregister(String service) throws Exception {
		registry.unbind(service);
	}
}
