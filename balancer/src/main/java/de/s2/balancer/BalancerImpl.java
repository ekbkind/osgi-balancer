package de.s2.balancer;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BalancerImpl implements Balancer {

	private static Logger logger = LoggerFactory.getLogger(BalancerImpl.class);
	
	private List<NodeInformation> availableNodes = new ArrayList<>();		
	private static int count = 0;

	
	public BalancerImpl(List<NodeInformation> availableNodes) {
		this.availableNodes = availableNodes;
	}
	
	public ClientConnection getNodeForService(Class<?> service) {

		ClientConnection result = null;
		if(availableNodes != null && availableNodes.size() > 0) {
			NodeInformation chosen = chooseNodeByExtremlyComplexAlgorithm(availableNodes);
			
			Node node  = chosen.getConnectorForService(Node.class).getServiceInterface();
			try {
				node.activateBundle(NodeInformation.computeServiceName(service));
			} catch (RemoteException e) {
				logger.error("Could not activate bundle");
			}
			
			result = chosen.getConnectorForService(service);
		}
		return result;
	}

	NodeInformation chooseNodeByExtremlyComplexAlgorithm(
			List<NodeInformation> availableNodes) {		
		int size = availableNodes.size();
		int index = count % size;
		NodeInformation chosen = availableNodes.get(index);
		
		return chosen;
	}
}
