package de.s2.balancer.osgi.balancer;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.s2.balancer.BalancerImpl;
import de.s2.balancer.NodeInformation;
import de.s2.balancer.RMIServerBean;

public class Activator implements BundleActivator {

	private static Logger logger = LoggerFactory.getLogger(Activator.class);

	private static final String SERVICE = "balancer";

	private RMIServerBean rmiServerBean;

	@Override
	public void start(BundleContext context) throws Exception {
		logger.info("Activating bundle balancer-balancer");

		logger.info("Read rmi port");
		int port;
		try {
			port = Integer.parseInt(context
					.getProperty("de.s2.balancer.balancer.port"));
		} catch (Exception e) {
			logger.error("Error while reading rmi port");
			port = 4710;
		}

		logger.info("Starting rmi on port {}", port);
		rmiServerBean = new RMIServerBean(port);

		List<NodeInformation> availableNodes = lookupAvailableNodes();
		
		logger.info("Register node bean");
		rmiServerBean.register(SERVICE, new BalancerImpl(availableNodes));

		logger.info("Bundle balancer-balancer activated");
	}

	//TODO: detection by hazelcast
	List<NodeInformation> lookupAvailableNodes() {
		List<NodeInformation> availableNodes = new ArrayList<>();
		NodeInformation localNode = new NodeInformation();
		localNode.setHost("localhost");
		localNode.setPort(4711);
		availableNodes.add(localNode);
		return availableNodes;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		logger.info("Stopping bundle");
		rmiServerBean.deregister(SERVICE);
		rmiServerBean.preDestroy();

		logger.info("bundle stopped");
	}
}