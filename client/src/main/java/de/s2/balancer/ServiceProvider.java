package de.s2.balancer;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ServiceProvider {
	private Balancer balancer;
	Registry registry;
	
	public ServiceProvider(String balancerHost, int balancerPort) throws Exception {
		registry = LocateRegistry.getRegistry(balancerHost, balancerPort);
	}
	
	public <T> T getService(Class<T> c){
		try {
			return getBalancer().getNodeForService(c).getServiceInterface();
		} catch (RemoteException e) {
			throw new IllegalStateException("Unable to obtain remote reference", e);
		}
	}
	
	private synchronized Balancer getBalancer() {
		if(balancer == null) {			
	        try {
				balancer = (Balancer) registry.lookup("balancer");
			} catch (Exception e) {
				throw new IllegalStateException("Could not lookup balancer");
			} 
		}
		
		return balancer;
	}
}
