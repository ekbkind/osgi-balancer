package de.s2.balancer;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class BalancerDriver {
	public static void main(String[] args) throws Exception{		
		Registry bean = LocateRegistry.getRegistry("localhost", 4710);

		Balancer balancer = (Balancer) bean.lookup("balancer");
		
//		ClientConnection cc = balancer.getNodeForService(Node.class);
//		Node node = cc.getServiceInterface();
//		
//		node.activateBundle("helloService");
//		node.activateBundle("echoService");	
		
		ClientConnection helloConnection = balancer.getNodeForService(HelloService.class);
		HelloService hello = helloConnection.getServiceInterface();
		System.err.println(hello.hello("remote world"));
	}
}
