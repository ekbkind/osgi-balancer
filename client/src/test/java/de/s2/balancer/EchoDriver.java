package de.s2.balancer;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class EchoDriver {
	public static void main(String[] args) throws Exception{		
		Registry bean = LocateRegistry.getRegistry("localhost", 4711);
		
		EchoService echo = (EchoService) bean.lookup("echoService");
		
		System.err.println(echo.echo("Hallo"));
	}
}
