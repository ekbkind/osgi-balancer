package de.s2.balancer;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class HelloDriver {
	public static void main(String[] args) throws Exception{		
		Registry bean = LocateRegistry.getRegistry("localhost", 4711);
		
		HelloService hello = (HelloService) bean.lookup("helloService");
		
		System.err.println(hello.hello("World"));
	}
}
