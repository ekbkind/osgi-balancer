package de.s2.balancer;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


public class NodeDriver {
	public static void main(String[] args) throws Exception{		
		Registry bean = LocateRegistry.getRegistry("localhost", 4711);

		Node node = (Node) bean.lookup("node");
		
		node.activateBundle("helloService");
		node.activateBundle("echoService");						
	}
}
