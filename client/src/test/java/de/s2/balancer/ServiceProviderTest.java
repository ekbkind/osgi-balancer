package de.s2.balancer;

import java.io.Serializable;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ServiceProviderTest {

	private static final class DummyBalancer implements Balancer, Serializable {

		private static final long serialVersionUID = 1L;

		private final String serviceName;
		private final int rmiPort;

		private DummyBalancer(String serviceName, int rmiPort) {
			this.serviceName = serviceName;
			this.rmiPort = rmiPort;
		}

		public ClientConnection getNodeForService(Class<?> service) {
			return new RMIClientConnector("localhost", rmiPort, serviceName);
		}
	}

	private RMIServerBean rmiServer;
	private ServiceProvider serviceProvider;

	@Before
	public void setUp() throws Throwable {
		final int rmiPort = 6666;
		final String serviceName = "helloService";
		rmiServer = new RMIServerBean(rmiPort);
		rmiServer. register(serviceName, new HelloServiceImpl());
		rmiServer.register("balancer",
				new DummyBalancer(serviceName, rmiPort));

		serviceProvider = new ServiceProvider("localhost", rmiPort);
	}

	@After
	public void destroy() throws Throwable {
		rmiServer.preDestroy();
	}

	@Test
	public void test() throws Exception {
		HelloService helloService = serviceProvider
				.getService(HelloService.class);
		
		System.err.println(helloService.getClass().getName());

		Assert.assertNotNull(helloService);
		String msg = "Welt";

		String result = helloService.hello(msg);

		Assert.assertNotNull(result);
		Assert.assertEquals("hello Welt", result);
	}

	@Test
	public void testNull() throws Exception {
		HelloService helloService = serviceProvider
				.getService(HelloService.class);

		Assert.assertNotNull(helloService);
		String msg = null;

		String result = helloService.hello(msg);

		Assert.assertNotNull(result);
		Assert.assertEquals("hello null", result);
	}

}
