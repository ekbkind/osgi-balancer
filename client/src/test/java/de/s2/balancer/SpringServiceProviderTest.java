package de.s2.balancer;

import java.io.Serializable;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/spring-context.xml"})
public class SpringServiceProviderTest {

	private static final class DummyBalancer implements Balancer, Serializable {

		private static final long serialVersionUID = 1L;

		private final String serviceName;
		private final int rmiPort;

		private DummyBalancer(String serviceName, int rmiPort) {
			this.serviceName = serviceName;
			this.rmiPort = rmiPort;
		}

		public ClientConnection getNodeForService(Class<?> service) {
			return new RMIClientConnector("localhost", rmiPort, serviceName);
		}
	}

	@Autowired
	private RMIServerBean rmiServer;
	
	@Autowired
	private ApplicationContext applicationContext;

	@Before
	public void setUp() throws Throwable {
		final int rmiPort = 7777;
		final String serviceName = "helloService";
		rmiServer.register(serviceName, new HelloServiceImpl());
		rmiServer.register("balancer",
				new DummyBalancer(serviceName, rmiPort));
	}

	@Test
	public void test() throws Exception {

		HelloService helloService = applicationContext.getBean("helloService", HelloService.class);
		
		Assert.assertNotNull(helloService);
		String msg = "Welt";

		String result = helloService.hello(msg);

		Assert.assertNotNull(result);
		Assert.assertEquals("hello Welt", result);
	}
}
