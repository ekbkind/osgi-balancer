package de.s2.balancer.osgi.echo;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import de.s2.balancer.EchoServiceImpl;

public class Activator implements BundleActivator {

	private static final String SERVICE = "echoService";

	@Override
	public void start(BundleContext context) throws Exception {
		de.s2.balancer.osgi.node.Activator.getInstance().register(SERVICE,
				new EchoServiceImpl());
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		de.s2.balancer.osgi.node.Activator.getInstance().deregister(SERVICE);
	}
}
