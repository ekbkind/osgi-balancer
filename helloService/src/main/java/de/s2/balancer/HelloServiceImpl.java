package de.s2.balancer;


public class HelloServiceImpl implements HelloService {

	@Override
	public String hello(String msg) {
		return "hello " + msg;
	}

}
