package de.s2.balancer.osgi.hello;



import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import de.s2.balancer.HelloServiceImpl;

public class Activator implements BundleActivator {

	private static final String SERVICE = "helloService";
	
	@Override
	public void start(BundleContext context) throws Exception {
		de.s2.balancer.osgi.node.Activator.getInstance().register(SERVICE, new HelloServiceImpl());
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		de.s2.balancer.osgi.node.Activator.getInstance().deregister(SERVICE);
	}
}
