package de.s2.balancer;

import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeImpl implements Node{

	private static Logger logger = LoggerFactory.getLogger(NodeImpl.class);

	private transient BundleContext bundleContext;
	
	private Map<String, String> bundlesById = new HashMap<>();
	
	public NodeImpl(BundleContext bundleContext) {
		this.bundleContext = bundleContext;
		logger.warn("Adding static bundle refs");
		// TODO: read via configuration
		bundlesById.put("helloService", "mvn:de.s2.balancer/balancer-hello/1.0.0-SNAPSHOT");
		bundlesById.put("echoService", "mvn:de.s2.balancer/balancer-echo/1.0.0-SNAPSHOT");
	}

	@Override
	public void activateBundle(String name) throws java.rmi.RemoteException{
		String bundleMavenLocation = bundlesById.get(name);
		if(bundleMavenLocation != null && !bundleMavenLocation.trim().isEmpty()) {
			try {
				if(bundleContext.getBundle(bundleMavenLocation) != null) {
					System.err.println("Found bundle");
				}
				logger.info("activating service {} with bundle {}", name, bundleMavenLocation);
				bundleContext.installBundle(bundleMavenLocation);
				bundleContext.getBundle(bundleMavenLocation).start();
			} catch (BundleException e) {
				logger.error("Could not load bundle {}", bundleMavenLocation, e);
			}
			
			
		}

	}
}
