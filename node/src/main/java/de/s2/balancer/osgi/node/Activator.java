package de.s2.balancer.osgi.node;

import java.rmi.Remote;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.s2.balancer.NodeImpl;
import de.s2.balancer.RMIServerBean;

public class Activator implements BundleActivator {
		
	private static Logger logger = LoggerFactory.getLogger(Activator.class);
	
	private static final String SERVICE = "node";
	
	private static Activator instance;
	private RMIServerBean rmiServerBean;
	
	@Override
	public void start(BundleContext context) throws Exception {
		logger.info("Activating bundle balancer-node");
		instance = this;
		
		logger.info("Read rmi port");
		int port;
		try {
			port = Integer.parseInt(context.getProperty("de.s2.balancer.node.port"));
		} catch (Exception e) {
			logger.error("Error while reading rmi port");
			port = 4711;
		}
		
		logger.info("Starting rmi on port {}", port);
		rmiServerBean = new RMIServerBean(port);
		
		logger.info("Register node bean");
		register(SERVICE, new NodeImpl(context));
				
		logger.info("Bundle balancer-node activated");
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		logger.info("Stopping bundle");
		deregister(SERVICE);
		rmiServerBean.preDestroy();
		
		logger.info("bundle stopped");
	}

	public <T extends Remote> void register(String service, T instance) throws Exception{                ;
		rmiServerBean.register(service, instance);
	}
	
	public void deregister(String service) throws Exception{
		rmiServerBean.deregister(service);
	}
	
	public static Activator getInstance() {
		return instance;
	}
}
